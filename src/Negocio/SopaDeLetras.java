package Negocio;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];
    private char letra[]=null;
    private String palabra="";
    private int total=0;
    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    
    
    
    
    
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    {
        boolean cuadrada=true;
        
        for(int i=0; i<sopas.length && cuadrada; i++)
        {
           if(sopas[i].length != sopas.length)   
                    cuadrada = false;
        }
        
        return cuadrada;
    }
    
    
    
    
     public boolean esDispersa()
    {
        boolean dispersa = false;
        
        if(esCuadrada()==false && esRectangular()==false)
            dispersa = true;
        
        return dispersa;
    }
    
     
     
    public boolean esRectangular()
    {
        boolean rectangular=true;
        
        int columnas = sopas[0].length;
        
        for(int i=0; i<sopas.length && rectangular; i++){
                
                if( (esCuadrada()==true) || (sopas[i].length != columnas))
                    rectangular = false;
                }
          
        return rectangular;
    }
    
    
    
    public void palabra(){
        
        this.letra= new char [this.palabra.length()];
        for(int i=0; i<letra.length; i++){  
            letra[i]=this.palabra.charAt(i);
        }
        
    }
    
    
    
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    public int getContar()
    {
        //char letra []= new char [palabra.length()];
        char result []= new char [letra.length];
        int total = 0;
        int contador=0;
        
        /*for(int i=0; i<letra.length; i++){  
            letra[i]=palabra.charAt(i);
        }*/
        
        
        for(int i=0; i<sopas.length;i++){
            
            for(int j=0; j<sopas[i].length;j++){
                
                if(contador<letra.length &&(sopas[i][j]==this.palabra.charAt(contador)) ){
                    result[contador] = letra[contador];
                    contador++;
                    
                }else{
                    contador=0;
                }
                
             }
                String pal = String.valueOf(letra);
                String pal2= String.valueOf(result);
                
                if(pal.equals(pal2)){
                    
                    total++;  
                }
                
                  contador=0;
        }
        
 	return total;
        
    }
    
    
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
        char diagonal [] = new char[sopas.length];
        
        if(esCuadrada()==false){
            throw new Exception("la matriz no es cuadrada, no se puede sacar diagonal ");
            
        }else if(esCuadrada()){
            for(int i = 0; i<sopas.length; i++){
               if(sopas[i]==sopas[i])
                        diagonal[i]= sopas[i][i];
                }
               
        }
        
        return  diagonal;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }
    
    
    
   
    
    
    public void leerExcel (FileInputStream ruta) throws Exception{
         
        int filas;
        int columnas;
              
        HSSFWorkbook archivoExcel = new HSSFWorkbook(ruta);
       
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
         
        filas =  hoja.getLastRowNum()+1;
        
             
        for (int i = 0; i < filas; i++) {            
                     
            HSSFRow filas2 = hoja.getRow(i);
           
            columnas = filas2.getLastCellNum();   
       
           sopas = new char [filas][columnas];
              
          for(int m=0; m<filas; m++){                 
           
            HSSFRow columnas2  = hoja.getRow(m);
            
            for(int s=0; s<columnas; s++){
                 
                String valor = columnas2.getCell(s).getStringCellValue();
                sopas[m][s]=valor.charAt(0);
                
             }             
          } 
       }
    }

        
        
    
    
    
    public void llenar (int i, int j, char letra){
        
        for(int k =0; k<sopas.length;k++){
            
            for(int l=0;l<sopas[k].length;l++){
                
                sopas[i][j]=letra;
            }
        }
    }
    
    
     
    public void imprimir() {

        for (char[] sopa : sopas) {
            System.out.println("");
            for (int j = 0; j < sopa.length; j++) {
                System.out.print(sopa[j] + "\t");
            }
        }

    }
    
    
    
    
    
    
    
    
    
    public int buscarHorizontal(){
       
      
        int palabras=0;
        char result []= new char [this.palabra.length()];
        
        int contador=0;
        
        for(int i=0; i<sopas.length;i++){
            //result= new char [this.palabra.length()];
            
            
            for(int j=0; j<sopas[i].length;j++){
                
                if(letra.length>contador &&(sopas[i][j]==palabra.charAt(contador)) ){
                    result[contador] = letra[contador];
                    contador++;
                    
                    String pal = String.valueOf(this.letra);
                    String pal2= String.valueOf(result);  
                    
                    
                   if(pal.equals(pal2)){
                        /*System.out.println("fila "  + i);
                        System.out.println("colu " + j);
                        System.out.println("contador " + contador);
                        System.out.println("pal2 " + pal2);
                        System.out.println("***********");*/
                        
                        for(int m=0; m<result.length;m++){
                            result[m]='.';
                        }
                    
                    
                    this.total++;
                    palabras++;
                    contador=0;
                    
                    }
                }else{
                    contador=0;
                    
                    if(sopas[i][j]==palabra.charAt(0)){
                        j--;
                    }
                }
                
             }
                   
                contador=0;
        }
        
        return palabras;
    }
    
    
    
    
    
    public int buscarHorizontalAlreves(){
       
        
        int palabras=0;
        char result []= new char [this.palabra.length()];
        int contador=0;
       
        
        for(int i=0; i<sopas.length;i++){
            result= new char [this.palabra.length()];
            
            for(int j=sopas[i].length-1; j>=0;j--){
                
                
                if(contador<letra.length && (sopas[i][j]==palabra.charAt(contador)) ){
                    result[contador] = letra[contador];
                    
                    contador++;
                  
                    
                    
                    String pal = String.valueOf(this.letra);
                    String pal2= String.valueOf(result);  
                    
                     /*System.out.println("pal2 " + pal2);
                     System.out.println("fila "  + (i+1));
                     System.out.println("colu " + (j+1));
                     System.out.println("*************");*/
                    
                   if(pal.equals(pal2)){
                       
                        for(int m=0; m<result.length;m++){
                            result[m]='.';
                        }
                    
                    
                    
                    palabras++;
                    contador=0;;
                    
                    }
                }else{
                    contador=0;
                    if(sopas[i][j]==palabra.charAt(0)){
                        j++;
                    }
                }
                
             }
                   
        }
        
        return palabras;
    }
    
    
    
    
    public int buscarPalabraDiagonalAlReves(String palabra){

        char vector[]=palabra.toCharArray();
        int filas=0;
        int columnas=0;
        int contador=0;
        boolean aux=false;
        int palabras=0;
        int posicion=vector.length-1;
        char vectorAlReves[]=new char[vector.length];
        
        for(int i=0;i<vectorAlReves.length;i++){
            vectorAlReves[i]=vector[posicion];
            System.out.print(vectorAlReves[i]);
            posicion--;
        }

        for(int i=0;i<this.sopas.length;i++){
            for(int j=0;j<sopas[0].length;j++){

                filas=i;
                columnas=j;
                contador=0;
                aux=false;

                while(contador!=vectorAlReves.length&&filas<sopas.length&&columnas<sopas[0].length&&columnas>=0&&filas>=0){

                    if(sopas[filas][columnas]==vectorAlReves[contador]){

                        filas++;//System.out.println("Filas:"+filas);
                        columnas++;//System.out.println("Columnas:"+columnas);
                        contador++;//System.out.println("Contador:"+contador);
                        aux=true;
                    }
                    else{aux=false;contador=vectorAlReves.length;
                    }
                }
                if(aux!=false&&contador==vector.length){

                    System.out.println("la palabra se encuentra diagonal de derecha a iquierda de abajo arriba fila: "+(filas+1)+" columna: "+(columnas+1));  
                    aux=false;
                    palabras++;
                }
            }
        }
        return palabras;
    }
    
    
    
    
    public int buscarPalabraDiagonalInferior(String palabra){

        char vector[]=palabra.toCharArray();
        int filas=0;
        int columnas=0;
        int contador=0;
        boolean aux=false;
        int palabras=0;

        for(int i=0;i<this.sopas.length;i++){
            for(int j=0;j<this.sopas[0].length;j++){

                filas=i;
                columnas=j;
                contador=0;
                aux=false;

                while(contador!=vector.length&&filas<this.sopas.length&&columnas>=0){

                    if(this.sopas[filas][columnas]==vector[contador]){

                        filas++;//System.out.println("Filas:"+filas);
                        columnas--;//System.out.println("Columnas:"+columnas);
                        contador++;//System.out.println("Contador:"+contador);
                        aux=true;

                    }else{aux=false;contador=vector.length;
                    }

                }

                if(aux!=false&&contador==vector.length){

                    System.out.println("la palabra se encuentra diagonal de derecha a izquierda de arriba abajo fila: "+(i+1)+" columna: "+(j+1));  
                    aux=false;
                    palabras++;

                }
            }
        }
        return palabras;
    }
    
    
    
    public int buscarPalabraDiagonalInferiorAlReves(String palabra){
        char vector[]=palabra.toCharArray();
        int filas=0;
        int columnas=0;
        int contador=0;
        boolean aux=false;
        int palabras=0;
        char vectorAlReves[]=new char[vector.length];
        int posicion=vector.length-1;

        for(int i=0;i<vectorAlReves.length;i++){

            vectorAlReves[i]=vector[posicion];
            posicion--;
        }

        
        for(int i=0;i<this.sopas.length;i++){
            for(int j=0;j<this.sopas[0].length;j++){

                filas=i;
                columnas=j;
                contador=0;
                aux=false;

                while(contador!=vector.length&&filas<this.sopas.length&&columnas>=0){

                    if(this.sopas[filas][columnas]==vectorAlReves[contador]){

                        filas++;System.out.println("Filas:"+filas);
                        columnas--;System.out.println("Columnas:"+columnas);
                        contador++;System.out.println("Contador:"+contador);
                        aux=true;

                    }else{aux=false;contador=vectorAlReves.length;
                    }

                }

                if(aux!=false&&contador==vectorAlReves.length){

                    System.out.println("la palabra se encuentra diagonal de derecha a izquierda de arriba abajo fila: "+(filas+1)+" columna: "+(columnas+1));  
                    aux=false;
                    palabras++;

                }
            }
        }
        return palabras;
    }
    
    
    
    public int buscarPalabraVerticalAlReves(String palabra){
         
        char vector[]=palabra.toCharArray();
        int filas=0;
        int contador=0;
        boolean aux=false;
        int palabras=0;
        char vectorAlReves[]=new char[vector.length];
        int posicion=vector.length-1;

        for(int i=0;i<vectorAlReves.length;i++){

            vectorAlReves[i]=vector[posicion];
            posicion--;
        }
        
        for(int i=0;i<this.sopas.length;i++){
            for(int j=0;j<this.sopas[0].length;j++){

                filas=i;
                contador=0;
                aux=false;

                while(contador!=vectorAlReves.length&&filas<this.sopas.length&&j<sopas[0].length){

                    if(this.sopas[filas][j]==vectorAlReves[contador]){

                        filas++;//System.out.println("Filas:"+filas);
                        contador++;//System.out.println("Contador:"+contador);
                        aux=true;

                    }else{aux=false;contador=vectorAlReves.length;
                    }

                }

                if(aux!=false&&contador==vectorAlReves.length){

                    System.out.println("la palabra se encuentra vertical de arriba abajo fila: "+(i+1)+" columna: "+(j+1));  
                    aux=false;
                    palabras++;

                }
            }
        }
        return palabras;
    }
    
    
    /*public int buscarVertical(){
        
        int contador=0;
        char result []= new char[palabra.length()];
        int total=0;
        int j=0;
        
        
        for(int i=0; i<sopas.length;i++){
            
            
            if((contador<=letra.length && i<sopas.length) && sopas[i][j]==this.palabra.charAt(contador) ){
                
                    result[contador]=letra[contador];
                    contador++;
                
                    String pal=String.valueOf(letra);
                    String pal2= String.valueOf(result);
                
                System.out.println("la pal2 " + pal2);
                System.out.println("fila " + (i+1));
                System.out.println("columna " + (j+1));
                
                if(pal.equals(pal2) ){
                    total++;
                    j++;
                    i=-1;
                    contador=0;
                    
                    for(int p=0; p<result.length;p++){
                        result[p]='.';
                    }
                    
                }
                
            }else
                contador=0;
            
            if(sopas.length<i){
                j++;
                i=-1;
                
                for(int p=0; p<result.length;p++){
                    result[p]= '.';
                }
                
            }
            
        }
        
        
       return total;
        
    }*/
    
    
    public int buscarVerticalAbajo(String palabra){
    int cantidad=0;
    int k=0;
    
    for(int i=0; i<sopas[0].length; i++){
        for(int j=0; j<sopas.length; j++){
           
            if(sopas[j][i]==palabra.charAt(k)){
            k++;
            }else{
            k=0;
            if(sopas[j][i]==palabra.charAt(k)){
            k++;
            }
            }
            
            
            if(k==palabra.length()){
            cantidad++;
            k=0;
            }else{
            if(k>0 && j==sopas[i].length-1){
                k=0;
            }
            }
        }        
        }
    
    return cantidad;
    
    }
    
    
    
    
    
    
    public int buscarPalabraDiagonal1(String palabra){
        
        char vector[]=palabra.toCharArray();
        int filas=0;
        int columnas=0;
        int contador=0;
        boolean aux=false;
        int palabras=0;
        for(int i=0;i<this.sopas.length;i++){
          for(int j=0;j<sopas[0].length;j++){
            filas=i;
            columnas=j;
            contador=0;
            aux=false;
        while(contador!=vector.length&&filas<sopas.length&&columnas<sopas[0].length){
            
            if(sopas[filas][columnas]==vector[contador]){
               filas++;//System.out.println("Filas:"+filas);
               columnas++;//System.out.println("Columnas:"+columnas);
               contador++;//System.out.println("Contador:"+contador);
               aux=true;
            }else{aux=false;contador=vector.length;}
        } 
        if(aux!=false&&contador==vector.length){
       
            System.out.println("la palabra se encuentra diagonal de izquierda a derecha de arriba abajo fila: "+(i+1)+" columna: "+(j+1));   
            aux=false;
            palabras++;
             }
          }
       }
        return palabras;
    }

    
    
    
    
    
    public int buscarVerticalArriba(String palabra){
    int cantidad=0;
    int k=0;
    
   for(int i=sopas[0].length-1; i>=0; i--){
        for(int j=sopas.length-1; j>=0; j--){
           
            if(sopas[j][i]==palabra.charAt(k)){
            k++;
            }else{
            k=0;
            if(sopas[j][i]==palabra.charAt(k)){
            k++;
            }
            }
            
            
            if(k==palabra.length()){
            cantidad++;
            k=0;
            }else{
            if(k>0 && j==sopas[i].length-1){
                k=0;
            }
            }
        }        
        }
    
    return cantidad;
    
    }
    
/***Diagonal al reves***/
  /*  public int buscarPalabraDiagonal1(String palabra) {

        char vector[] = palabra.toCharArray();
        int filas = 0;
        int columnas = 0;
        int contador = 0;
        boolean aux = false;
        int columnasP = 0;
        int filasP = 0;
        int palabras = 0;
        for (int i = sopas.length - 1; i < this.sopas.length; i++) {
            for (int j = sopas.length - 1; j < sopas[0].length; j++) {
                filas = i;
                columnas = j;
                contador = 0;
                aux = false;
                columnasP = columnas;
                filasP = filas;
                
                        filas--;     System.out.println("Filas:"+filas);
                        columnas--;  System.out.println("Columnas:"+columnas);
                        contador++;  System.out.println("Contador:"+contador);

                while (contador != vector.length && filas < sopas.length && columnas < sopas[0].length) {

                    if (sopas[filas][columnas] == vector[contador]) {
                        filas--;     System.out.println("Filas:"+filas);
                        columnas--;  System.out.println("Columnas:"+columnas);
                        contador++;  System.out.println("Contador:"+contador);
                        aux = true;
                    } else {
                        aux = false;
                        contador = vector.length;
                    }
                }
                if (aux != false && contador == vector.length) {

                    System.out.println("la palabra se encuentra diagonal de derecha a izquierda abajo arriba fila: " + (filasP + 1) + " columna: " + (columnasP + 1));
                    aux = false;
                    palabras++;
                }
            }
        }
        return palabras;
    }*/

}


    
    
    
    
    
    
   


